package br.com.mastertech.imersivo.controller

import br.com.mastertech.imersivo.kafka.models.RqCompany
import br.com.mastertech.imersivo.kafka.models.RsCompany
import br.com.mastertech.imersivo.kafka.models.fromRqToModel
import br.com.mastertech.imersivo.kafka.models.fromRqToRs
import br.com.mastertech.imersivo.service.PublishCompany
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*


@RestController
class CompanyController {

    @Autowired
    lateinit var service: PublishCompany


    @PostMapping("empresa/cadastro")
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody rqCompany: RqCompany) : RsCompany {
        service.publish(rqCompany.fromRqToModel())
        return rqCompany.fromRqToRs()
    }
}