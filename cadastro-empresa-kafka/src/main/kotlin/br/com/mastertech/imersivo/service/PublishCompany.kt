package br.com.mastertech.imersivo.service

import br.com.mastertech.imersivo.kafka.models.Company
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.stereotype.Service


@Service
class PublishCompany {
    companion object {
        private const val TOPIC = "chrislucas-biro-1"
    }

    @Autowired
    private lateinit var publisher: KafkaTemplate<String, Company>

    fun publish(company: Company) = publisher.send(TOPIC, company)
}