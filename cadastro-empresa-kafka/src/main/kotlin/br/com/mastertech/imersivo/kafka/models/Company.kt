package br.com.mastertech.imersivo.kafka.models

data class Company(val cnpj: String)