package br.com.mastertech.imersivo.kafka.models

import com.fasterxml.jackson.annotation.JsonProperty

class RqCompany(@JsonProperty("cnpj") val cnpj: String)