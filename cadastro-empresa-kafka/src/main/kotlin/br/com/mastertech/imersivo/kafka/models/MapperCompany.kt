package br.com.mastertech.imersivo.kafka.models


fun RqCompany.fromRqToRs() : RsCompany = RsCompany(this.cnpj)

fun RqCompany.fromRqToModel() : Company = Company(this.cnpj)